import Info from "./Info";
import React from "react";
import { Divider } from "antd";

export default function PreferencesData() {
  return (
    <>
      {preferences.map((data) => {
        return (
          <>
            <Info
              title={data.title}
              description={data.description}
              btnValue={data.btnValue}
            />
            <Divider />
          </>
        );
      })}
    </>
  );
}
const preferences = [
  {
    title: "Language",
    description: "English",
    btnValue: "Change",
  },
  {
    title: "Time Zone",
    description: "Central Standard Time(UTC-6)",
    btnValue: "Change",
  },
  {
    title: "Currency",
    description: "$(US Dollars)",
    btnValue: "Change",
  },
  {
    title: "Email Subscription",
    description: "Off",
    btnValue: "Change",
  },
  {
    title: "SMS Notification",
    description: "On",
    btnValue: "Change",
  },
];
