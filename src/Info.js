import React from "react";
import { Card, Col, Row, Button } from "antd";
import "./App.css";
const { Meta } = Card;

const Info = (Props) => {
  return (
    <Row>
      <Col span={12}>
        <Meta title={Props.title} description={Props.description} />
      </Col>
      <Col span={6} offset={6}>
        <Button className="btn">{Props.btnValue}</Button>
      </Col>
    </Row>
  );
};
export default Info;
