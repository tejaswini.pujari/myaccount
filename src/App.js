import React from "react";
import "antd/dist/antd.min.css";
// import "antd/dist/antd.css";
import "./index.css";
import Info from "./Info";
import { Card, Col, Row, Divider, Button } from "antd";
import Avatar from "antd/lib/avatar/avatar";
import { UserOutlined, SecurityScanOutlined } from "@ant-design/icons";
import PreferencesData from "./PreferencesData";
import ProfileData from "./ProfileData";
import SecurityData from "./SecurityData";
import PaymentData from "./PaymentData";

const { Meta } = Card;
const style = { backgroundColor: "rgb(181, 250, 181)", color: "green" };
export default function App() {
  return (
    <div className="site-card-wrapper">
      <Row gutter={[16, 48]}>
        <Col className="title" span={24}>
          My Account
        </Col>
      </Row>

      <Row className="container" gutter={[24, 24]}>
        <Col xs={24} md={12} lg={12} xl={12}>
          <Card>
            <Meta
              className="cardtitle"
              avatar={
                <Avatar size="medium" icon={<UserOutlined />} style={style} />
              }
              title="Profile"
            />

            <Info
              title="Photo"
              description={<Avatar src="https://joeschmoe.io/api/v1/random" />}
              btnValue="Change"
            />
            <Divider />
            <ProfileData />
            <Button className="btn"> Manage Profile</Button>
          </Card>
        </Col>

        <Col xs={24} md={12} lg={12} xl={12}>
          <Card>
            <Meta
              className="cardtitle"
              avatar={
                <Avatar size="large" icon={<UserOutlined />} style={style} />
              }
              title="Preferences"
            />

            <PreferencesData />
            <Button className="btn"> Manage Preferences</Button>
          </Card>
        </Col>
        <Col xs={24} md={12} lg={12} xl={12}>
          <Card>
            <Meta
              className="cardtitle"
              avatar={
                <Avatar size="large" icon={<UserOutlined />} style={style} />
              }
              title="Security"
            />

            <SecurityData />
            <Button className="btn"> Manage Security</Button>
          </Card>
        </Col>

        <Col xs={24} md={12} lg={12} xl={12}>
          <Card>
            <Meta
              className="cardtitle"
              avatar={
                <Avatar
                  size="large"
                  icon={<SecurityScanOutlined />}
                  style={style}
                />
              }
              title="Security"
            />

            <PaymentData />
            <Button className="btn"> Manage Security</Button>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
