import Info from "./Info";
import React from "react";
import { Divider } from "antd";

export default function SecurityData() {
  return (
    <>
      {security.map((data) => {
        return (
          <>
            <Info
              title={data.title}
              description={data.description}
              btnValue={data.btnValue}
            />
            <Divider />
          </>
        );
      })}
    </>
  );
}
const security = [
  {
    title: "Password",
    description: "********",
    btnValue: "Change",
  },
  {
    title: "Two-Factor Authentication",
    description: "You havent set up two-factor authentication.",
    btnValue: "Set Up",
  },
];
