import Info from "./Info";
import React from "react";
import { Divider } from "antd";
import "./App.css";
export default function ProfileData() {
  return (
    <>
      {profile.map((data) => {
        return (
          <>
            <Info
              title={data.title}
              description={data.description}
              btnValue={data.btnValue}
            />
            <Divider />
          </>
        );
      })}
    </>
  );
}
const profile = [
  {
    title: "Name",
    description: "James Doe",
    btnValue: "Change",
  },
  {
    title: "Email",
    description: "james.doe@website.com",
    btnValue: "Change",
  },
  {
    title: "Website",
    description: "https://johndoewebsite.com",
    btnValue: "Change",
  },
  {
    title: "Location",
    description: "New York",
    btnValue: "Change",
  },
];
