import Info from "./Info";
import React from "react";
import { Divider } from "antd";

export default function PaymentData() {
  return (
    <>
      {payment.map((data) => {
        return (
          <>
            <Info
              title={data.title}
              description={data.description}
              btnValue={data.btnValue}
            />
            <Divider />
          </>
        );
      })}
    </>
  );
}
const payment = [
  {
    title: "Credit/Debit Card",
    description: "1234*******5678",
    btnValue: "Edit",
  },
  {
    title: "PayPal",
    description: "Not connected",
    btnValue: "Connect",
  },
];
